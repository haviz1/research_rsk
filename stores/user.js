import { defineStore } from 'pinia'

export const useUserStore = defineStore('user', () => {
  const authToken = ref(null)
  const userData = ref()

  const setToken = (payload) => {
    authToken.value = payload
  }

  const setUserData = (payload) => {
    userData.value = payload
  }

  const removeToken = () => {
    authToken.value = null
  }

  const removeUserData = () => {
    userData.value = null
  }

  return {
    authToken,
    userData,
    setToken,
    removeToken,
    setUserData,
    removeUserData
  }
})