import { useUserStore } from '@/stores/user'

export default defineNuxtRouteMiddleware((to, from) => {
  const { authToken } = useUserStore()

  if (!authToken) {
    return navigateTo('/masuk')
  }
})