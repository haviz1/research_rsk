import { useUserStore } from '@/stores/user';

export default defineNuxtRouteMiddleware(async () => {
  const user = useUserStore()
  const token = useCookie('authToken')

  const { data } = await $fetch('https://devel.rumahsiapkerja.com/rsk-backend/v1/user/20e2ec7a-9e4e-4069-8413-6d48321af99d', {
    headers: {
      Authorization: `Bearer ${token.value}`
    }
  })

  user.setUserData(data)
  user.setToken(token.value)
})