export default defineNuxtPlugin(() => {
  return {
    provide: {
      seo: obj => ({
        title: obj.title,
        meta: [{
          property: 'title', 
          name: 'title', 
          content: obj.title
        }, {
          property: 'description',
          name: 'description',
          content: obj.description,
        }, {
          property: 'keywords',
          name: 'keywords',
          content: obj.keywords,
        }, {
          property: 'og:site_name', // START --- Open Graph / Facebook META
          name: 'og:site_name',
          content: obj.title,
        }, {
          property: 'og:type',
          name: 'og:type',
          content: 'website',
        }, { 
          property: 'og:url', 
          name: 'og:url', 
          content: obj.url 
        }, {
          property: 'og:title',
          name: 'og:title',
          content: obj.title,
        }, {
          property: 'og:description',
          name: 'og:description',
          content: obj.description,
        }, { 
          property: 'og:image', 
          name: 'og:image', 
          content: obj.image
        }, {
          property: 'twitter:card', // START --- TWITTER META
          name: 'twitter:card',
          content: 'summary_large_image',
        },
        {
          property: 'twitter:url',
          name: 'twitter:url',
          content: obj.url,
        },
        {
          property: 'twitter:title',
          name: 'twitter:title',
          content: obj.title,
        },
        {
          property: 'twitter:description',
          name: 'twitter:description',
          content: obj.description,
        },
        {
          property: 'twitter:site',
          name: 'twitter:site',
          content: '@im_haviz',
        },
        { 
          property: 'twitter:image', 
          name: 'twitter:image', 
          content: obj.image
        },
        {
          property: 'twitter:creator',
          name: 'twitter:creator',
          content: '@im_haviz',
        }]
      })
    }
  }
})